
$(document).on('input', '.invite__input, .invite__mail', function (e) {
    $(this).removeClass('error')
})


$("#form_add").submit(function (e) {
    e.preventDefault();

    console.log(e);

    let name = $('#name').val(),
        phone = $('#phone').val(),
        message = $('#message'),
        koment = $('#textareaMail').val();




    $.ajax({
        type: "POST",
        url: "/send.php",
        data: "name=" + name + "&tel=" + phone + "&koment=" + koment,
        success: function (html) {
            if (html == '1') {
                $('#form_add').hide();
                $('#message').addClass('active').append("<p class='otvet'>Ваше сообщение отпрвлено!</p>");
            } else {
                $('.otvet').empty();
                $('#message').hide();

                if(name.length===0){
                    $('#name').addClass('error')
                }

                if(phone.length===0){
                    $('#phone').addClass('error')
                }

                if(koment.length===0){
                    $('#textareaMail').addClass('error')
                }

                $('.invite__message').prepend("<p class='otvet'>Некоторые поля небыли заплонены</p>");
            }
        }
    });

});


let textareaMail = document.getElementById("textareaMail");

(function (e) {
// increase height value
    textareaMail.oninput = function () {
        //Get current style value
        let Style = (window.getComputedStyle) ? getComputedStyle(this, null) : this.currentStyle;
        //Get element height
        let Height = this.scrollHeight ? this.scrollHeight : this.offsetHeight ? this.offsetHeight : this.clientHeight;
        this.style.height = Height - parseInt(Style.paddingTop) - parseInt(Style.paddingBottom) + 'px';
    }
// decrease height value
    textareaMail.onkeydown = function (e) {
        let Style = (window.getComputedStyle) ? getComputedStyle(this, null) : this.currentStyle;
        let Height = this.scrollHeight ? this.scrollHeight : this.offsetHeight ? this.offsetHeight : this.clientHeight;
        if (e.keyCode === 8) {
            if (this.value === "") this.style.height = Style.fontSize;//reset height by default font-size value
            else this.style.height = Height - parseInt(Style.paddingTop) - parseInt(Style.paddingBottom) - parseInt(Style.fontSize) + 'px';
        }
    }
})();